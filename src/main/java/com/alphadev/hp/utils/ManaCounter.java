package com.alphadev.hp.utils;

import com.alphadev.hp.HarryPotter;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class ManaCounter {

    private Player player;
    private Plugin plugin = HarryPotter.getPlugin(HarryPotter.class);
    private  static ManaCounter manaCounter;

    public ManaCounter(Player player) {
        this.player = player;
        manaActivator();
    }

    public static  ManaCounter  GetInstance(Player player){
        if(manaCounter == null){
            manaCounter =  new ManaCounter(player);
        }
        return manaCounter;
    }

    public void manaActivator(){
        new BukkitRunnable() {
            public void run() {
                if(player.getExp() < 1){
                    try {
                        player.setExp(player.getExp()+0.01F);
                    }catch (Exception e){
                    }
                }
            }
        }.runTaskTimer(plugin,0,3);
    }


}
