package com.alphadev.hp.utils;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.*;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class SignInteracts {


    public void createMagicWandGetter(SignChangeEvent sign){
        if(sign.getPlayer().isOp()){
            if (sign.getLine(0).equalsIgnoreCase("[Magic Wand]") || sign.getLine(0).equalsIgnoreCase("[MagicWand]") ||  sign.getLine(0).equalsIgnoreCase("[mw]")) {
                sign.setLine(0, ChatColor.BOLD+""+ChatColor.LIGHT_PURPLE+"["+ChatColor.GREEN+"Magic Wand"+ChatColor.LIGHT_PURPLE+"]");

                String nameColor = "";
                Character tier = sign.getLine(1).toUpperCase().toCharArray()[0];
                switch (tier){
                    case 'N':
                        nameColor = ChatColor.BOLD+""+ChatColor.GREEN+sign.getLine(2);
                        break;
                    case 'U':
                        nameColor = ChatColor.BOLD+""+ChatColor.BLUE+sign.getLine(2);
                        break;
                    case 'R':
                        nameColor = ChatColor.BOLD+""+ChatColor.LIGHT_PURPLE+sign.getLine(2);
                        break;
                    case 'L':
                        nameColor = ChatColor.BOLD+""+ChatColor.GOLD+sign.getLine(2);
                        break;
                }
                Location loc = new Location(sign.getBlock().getWorld(), sign.getBlock().getLocation().getX()  , sign.getBlock().getLocation().getY()-1, sign.getBlock().getLocation().getZ());

                loc.getWorld().spawnParticle(Particle.FIREWORKS_SPARK,loc,12,1,1,1);
                for(Entity e :  loc.getWorld().getNearbyEntities(loc, 1, 1,1)){
                    if(e instanceof  ItemFrame){
                        if(((ItemFrame) e).getItem().getType() == Material.AIR){
                            sign.getPlayer().sendMessage(ChatColor.RED+"God is seeing what you are doing .. place an item inside an item frame first to continue.");
                            sign.getPlayer().playSound(sign.getPlayer().getLocation(), Sound.ENTITY_VILLAGER_NO, 1 ,1);
                            e.remove();
                            sign.getBlock().setType(Material.AIR);
                            sign.getPlayer().playSound(sign.getPlayer().getLocation(), Sound.BLOCK_STONE_BREAK, 1 ,1);
                        }else{
                            ((ItemFrame) e).setItem(MagicWand.magicWand(tier,nameColor,((ItemFrame) e).getItem().getType()));
                        }
                    }
                }
            }
        }
    }

    public void buyMagicWand(PlayerInteractEvent event){
        Player player = event.getPlayer();

        if(event.getAction()== Action.RIGHT_CLICK_BLOCK){
            Block sign = event.getClickedBlock();
            if(sign.getState() instanceof  Sign){
                BlockState stateBlock = sign.getState();
                Sign s = (Sign) stateBlock;
                if( s.getLine(0).equalsIgnoreCase(ChatColor.LIGHT_PURPLE+"["+ChatColor.GREEN+"Magic Wand"+ChatColor.LIGHT_PURPLE+"]")){
                    Location loc = new Location(s.getWorld(), s.getLocation().getX(), s.getLocation().getY() - 1, s.getLocation().getZ());
                    for (Entity e : loc.getWorld().getNearbyEntities(loc, 1, 1, 1)) {

                        if (e instanceof ItemFrame) {
                            if (((ItemFrame) e).getItem().getType() == Material.AIR) {
                                player.sendMessage(ChatColor.RED + "Are you kidding me ?.. place an item inside an item frame below the sign.. no ..no remake it!");
                                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1, 1);
                                e.remove();
                                s.setType(Material.AIR);
                                player.playSound(player.getLocation(), Sound.BLOCK_STONE_BREAK, 1, 1);
                            } else {

                                if( ChatColor.stripColor(((ItemFrame) e).getItem().getItemMeta().getDisplayName()).equalsIgnoreCase(s.getLine(2))){
                                    if(!player.getInventory().contains(((ItemFrame) e).getItem())){
                                        player.getInventory().addItem(((ItemFrame) e).getItem());
                                    }else{
                                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1, 1);
                                        player.sendMessage(ChatColor.RED + "You already have a wand ... smartass");
                                    }
                                }else{
                                    player.sendMessage(ChatColor.RED + "Don't cheat ... remake it! Put the right item on Item Frame");
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1, 1);
                                    e.remove();
                                    s.setType(Material.AIR);
                                    player.playSound(player.getLocation(), Sound.BLOCK_STONE_BREAK, 1, 1);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
