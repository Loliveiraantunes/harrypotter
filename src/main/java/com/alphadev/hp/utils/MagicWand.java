package com.alphadev.hp.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class MagicWand {

    public static ItemStack magicWand(Character tier,String name, Material material){

        if(tier == null){
            tier = 'N';
        }

        if(material == null){
            material = Material.STICK;
        }

        if(name ==null){
            name = ChatColor.BOLD+""+ChatColor.GREEN+"Ollivander's Wand";
        }

        ItemStack magicWand = new ItemStack(material);
        ItemMeta magicWandMeta = magicWand.getItemMeta();
        List<String> lore = new ArrayList<String>();

        switch (tier){
            case 'N':
                magicWandMeta.setDisplayName(name);
                lore.add("Tier: "+ChatColor.BOLD+""+ChatColor.GREEN+"NORMAL");
                lore.add("Multiply: "+ChatColor.BOLD+""+ChatColor.GRAY+"1x");
            break;

            case 'U':
                magicWandMeta.setDisplayName(name);
                lore.add("Tier: "+ChatColor.BOLD+""+ChatColor.BLUE+"UNCOMMON");
                lore.add("Multiply: "+ChatColor.BOLD+""+ChatColor.GRAY+"1.3x");
                break;

            case 'R':
                magicWandMeta.setDisplayName(name);
                lore.add("Tier: "+ChatColor.BOLD+""+ChatColor.LIGHT_PURPLE+"RARE");
                lore.add("Multiply: "+ChatColor.BOLD+""+ChatColor.GRAY+"1.8x");
                break;

            case 'L':
                magicWandMeta.setDisplayName(name);
                lore.add("Tier: "+ChatColor.BOLD+""+ChatColor.GOLD+"LEGENDARY");
                lore.add("Multiply: "+ChatColor.BOLD+""+ChatColor.GRAY+"2x");
                break;
        }
        lore.add("Description: "+ChatColor.GRAY+"You can cast different types of spells,");
        lore.add(ChatColor.GRAY+"spells will be multiplied by their level rarity.");
        lore.add(ChatColor.GRAY+"Modifying damage, duration and area.");
        lore.add(ChatColor.WHITE+"MagicWand");

        magicWandMeta.setLore(lore);
        magicWandMeta.addEnchant(Enchantment.BINDING_CURSE,1,true);
        magicWand.setItemMeta(magicWandMeta);

        return  magicWand;
    }


    @SuppressWarnings("deprecation")
    public static  void  getMagicWand(Player player){
      //  player.getInventory().setItem(2,magicWand(null,null));
        boolean hasMagicWand = false;


        for(int i = 0 ; i < player.getInventory().getContents().length; i ++){
           try {
                if (player.getInventory().getContents()[i].getItemMeta().getLore().get(player.getInventory().getContents()[i].getItemMeta().getLore().size()-1).equalsIgnoreCase(ChatColor.WHITE+"MagicWand")){
                hasMagicWand = true;
            }
           }catch (Exception e){
           }
        }
       if(hasMagicWand == false){
            player.getInventory().addItem(magicWand(null,null,null));
           player.sendMessage(ChatColor.GREEN+" You got a magic wand ... good, good .. very good.");
        }
    }

}
