package com.alphadev.hp.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class BookInteracts {

   private Inventory inventory;
   private List<ItemStack> menuItens;


    public static ItemStack grimory(){
        ItemStack book = new ItemStack(Material.BOOK);
        ItemMeta bookMeta = book.getItemMeta();
        List<String> lore = new ArrayList<String>();
        lore.add("Tier: "+ChatColor.BOLD+""+ChatColor.AQUA+"MAIN");
        lore.add(ChatColor.GREEN+"Grimory - Can learn differents spells using this book");
        bookMeta.setLore(lore);
        bookMeta.setDisplayName(ChatColor.BOLD+""+ChatColor.AQUA+"Grimory");
        bookMeta.addEnchant(Enchantment.ARROW_INFINITE,1,true);
        book.setItemMeta(bookMeta);
        return  book;
    }

    public static void getGrimory(Player player){
        if(!player.getInventory().contains(grimory())){
            player.getInventory().addItem(grimory());
        }
    }


    public void listMenuItens(){
        menuItens = new ArrayList<ItemStack>();
        ItemStack b1 = new ItemStack(Material.BOOK);
        ItemMeta b1Meta = b1.getItemMeta();
        b1Meta.setDisplayName("MAGIC 01");
        b1Meta.addEnchant(Enchantment.MENDING,100,true);
        b1.setItemMeta(b1Meta);

        menuItens.add(b1);

        ItemStack b2 = new ItemStack(Material.BOOK);
        ItemMeta b2Meta = b2.getItemMeta();
        b2Meta.setDisplayName("MAGIC 02");
        b2Meta.addEnchant(Enchantment.MENDING,100,true);
        b2.setItemMeta(b2Meta);

        menuItens.add(b2);

        ItemStack b3 = new ItemStack(Material.BOOK);
        ItemMeta b3Meta = b3.getItemMeta();
        b3Meta.setDisplayName("MAGIC 03");
        b3Meta.addEnchant(Enchantment.MENDING,100,true);
        b3.setItemMeta(b3Meta);

        menuItens.add(b3);
    }


    @SuppressWarnings("deprecation")
    public void grimoryInteract(Player player){
        if(player.getItemInHand().isSimilar(grimory())){
            inventory = Bukkit.createInventory(null, InventoryType.CHEST,ChatColor.BOLD+""+ChatColor.DARK_GRAY+"Grimory");
            listMenuItens();
            inventory.setItem(0,menuItens.get(0));
            inventory.setItem(2,menuItens.get(1));
            inventory.setItem(4,menuItens.get(2));

            player.openInventory(inventory);
        }
    }


    public  void inventoryInteract(InventoryClickEvent event){
        if (event.getInventory().getTitle().equalsIgnoreCase(ChatColor.BOLD+""+ChatColor.DARK_GRAY+"Grimory")){
            ItemStack clicked = event.getCurrentItem();
            event.setCancelled(true);
        }
    }

}
