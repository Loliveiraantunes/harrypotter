package com.alphadev.hp;

import com.alphadev.hp.events.BasicInteractions;
import org.bukkit.ChatColor;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class HarryPotter  extends JavaPlugin {

    private PluginManager pm = this.getServer().getPluginManager();

    @Override
    public void onEnable() {
       pm.registerEvents(new BasicInteractions(), this);
    }

    @Override
    public void onLoad() {
        greetings();
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll();
    }



    public void greetings(){
        this.getServer().getLogger().info(ChatColor.BOLD+""+ChatColor.GREEN+"Harry Potter - Plugin");
    }
}
