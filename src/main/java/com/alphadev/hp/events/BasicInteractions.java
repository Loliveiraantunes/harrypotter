package com.alphadev.hp.events;

import com.alphadev.hp.utils.BookInteracts;
import com.alphadev.hp.utils.MagicWand;
import com.alphadev.hp.utils.ManaCounter;
import com.alphadev.hp.utils.SignInteracts;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class BasicInteractions implements Listener {

    @EventHandler
    public void loadOnServer(PlayerMoveEvent ev){
        Player player = ev.getPlayer();
        ManaCounter.GetInstance(player);
        BookInteracts.getGrimory(player);
        MagicWand.getMagicWand(player);
    }

    @EventHandler
    public  void grimoryInteract(PlayerInteractEvent ev){
        Player player = ev.getPlayer();
        new BookInteracts().grimoryInteract(player);
        new SignInteracts().buyMagicWand(ev);
    }


    @EventHandler
    public  void menuInteract(InventoryClickEvent event){
        new BookInteracts().inventoryInteract(event);
    }

    @EventHandler
    public  void SignInteract(SignChangeEvent sing){
        new SignInteracts().createMagicWandGetter(sing);
    }

}
